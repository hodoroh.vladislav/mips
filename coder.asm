.data
    ask_input_msg:  .asciiz "Nom fichier d'entrer\n"
    ask_output_msg: .asciiz "Nom fichier sortie\n"
    ask_message: .asciiz "message 256bytes"
    header:  .space 54
    input_file: .space 128
    output_file: .space 128
    message_body: .space 256
    input_err:  .asciiz "\nImage non trouvee\n"
    output_err: .asciiz "\nFin erreur\n"
    buff:   .space 1


.text
main:


    #print ask_input_msg
    li $vo, 4
    la $a0, ask_input_msg
    syscall

    #read filename
    li $v0, 8
    la $a0, input_file
    li a1, 128
    syscall

    #print ask_output_msg
    li $v0, 4
    la $a0, ask_output_msg
    syscall

    #read output_file name

    li $v0, 8
    la $a0, output_file
    li $a0, 128
    syscall

    #ask message
    li $v0, 4
    la $a0, ask_message
    syscall

    li $v0, 8
    la $a0, message_body
    li $a0, 256
    syscall


