#include <stdio.h>
#include <stdlib.h>

int n_b (char *addr, int i) {
  char target_char = addr[i/8];
  return target_char >> i%8 & 0x1;
}

char d2c (int n){
  return (n<0 ? '?' : (n<10 ? '0'+n : (n<36 ? 'A' + (n-10) : '?')));
}

long long int P2(int n) {
  return 1 << n;
}

// Fonctions à compléter

// Exo 1
void base2(int n) {
}

// Exo 3
void baseB(int B, int n){
}

// Exo 4
void mantisse(float f, int result[]) {
}

float mantisseNormalisee(float f){
  return 0.f;
}

void exposant(float f, int result[]) {
}

int exposantSansExces(float f) {
  return 0;
}

int signe(float f) {
  return 0;
}

int main(){
  return EXIT_SUCCESS;
}
